%w(kpmg pwc).each do |name|
  company = Company.create(name: name.upcase)
  user = User.create(email: "john@#{name}.com", password: '123456', password_confirmation: '123456', company: company)

  10.times do |i|
    Quote.create(name: "Quote with number #{i}", company: company)
  end
end
